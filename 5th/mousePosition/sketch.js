
function setup() {
  
  createCanvas(900, 500);
  
}

function draw() {
  background(255);

  fill(color(255, 0, 0));

  // mouseX : カーソルのx座標
  // mouseY : カーソルのy座標
  ellipse(mouseX, mouseY, 30, 30);
}

