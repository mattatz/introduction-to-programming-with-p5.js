
function setup() {
  
  createCanvas(500, 500);
  
}

function draw() {
  background(255);

}

function keyPressed() {
  println("key pressed");
  fill(color(255, 0, 0));
  rect(width / 5, height / 2, 50, 50);
}

function keyReleased() {
  println("key released");
  fill(color(0, 255, 0));
  rect(width / 5 * 2, height / 2, 50, 50);
}

function keyTyped() {
  println("key typed");
  fill(color(0, 0, 255));
  rect(width / 5 * 3, height / 2, 50, 50);
}

