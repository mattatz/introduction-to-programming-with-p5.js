
var displayText = "HELLO";

function setup() {
  
  createCanvas(500, 500);

  textFont("Helvetica");
  textSize(20);
  
}

function draw() {

  background(255);

  noStroke();
  fill(color(255, 0, 0));
  text(displayText, 30, height / 2);

}

function keyPressed() {
}

function keyReleased() {
}

function keyTyped() {
  displayText += key;
}

