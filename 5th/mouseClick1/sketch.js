
var previousMouseIsPressed = false;

function setup() {
  
  createCanvas(500, 500);
  
}

function draw() {
  background(255);

  // 「クリックを判別する条件分岐」
  // 前回のdraw処理内でのmouseIsPressedと
  // 現在のdraw処理内でのmouseIsPressedが異なり、
  // かつ、
  // mouseが押されていない場合
  if(previousMouseIsPressed != mouseIsPressed && !mouseIsPressed) {
      // 画面真ん中に正方形を描く
      rect(width / 2, height / 2, 100, 100);
  }

  previousMouseIsPressed = mouseIsPressed;

}

