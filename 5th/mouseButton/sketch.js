
function setup() {
  
  createCanvas(500, 500);
  
}

function draw() {
  background(255);

  // マウスが押された時
  if(mouseIsPressed) {
    fill(color(255, 0, 0));
    rect(100, 100, 50, 50);
  }

  // マウスが押された時
  if(mouseIsPressed) {
    if(mouseButton == LEFT) { // 押されたボタンが左ボタンの時
      fill(color(0, 255, 0));
    } else if(mouseButton == CENTER) { // 押されたボタンが真ん中ボタンの時
      fill(color(0, 0, 255));
    } else if(mouseButton == RIGHT) { // 押されたボタンが右ボタンの時
      fill(color(0, 255, 255));
    } 
    rect(200, 100, 50, 50);
  }

}

