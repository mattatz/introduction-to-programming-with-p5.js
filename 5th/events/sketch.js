
function setup() {
  
  createCanvas(500, 500);
  
}

function draw() {
}

function mouseMoved() {
  println("mouse moved");
}

function mouseDragged() {
  println("mouse dragged");
}

function mousePressed() {
  println("mouse pressed");
}

function mouseReleased() {
  println("mouse released");
}

function mouseClicked() {
  println("mouse clicked");
}

function mouseWheel(event) {
  // event.deltaにスクロールした量が格納されている
  println("mouse wheel");
}

