function setup() {
  createCanvas(500, 500);
  background(255);
  
  fill(255, 0, 0); // 塗りつぶす色の指定
  rect(50, 50, 80, 40); // 長方形を描画
  
  fill(0, 255, 0);
  rect(135, 50, 20, 80);
  
  fill(0, 0, 255);
  rect(50, 95, 80, 35);
  
  noFill(); // 塗りつぶさない

  stroke(255, 0, 0); // 線の色を指定
  rect(200, 50, 80, 40); // 長方形を描画

  stroke(0, 255, 0);
  rect(285, 50, 20, 80);
  
  stroke(0, 0, 255);
  rect(200, 95, 80, 35);
  
  noStroke();
  
  fill(100, 200, 80);
  ellipse(150, 200, 80, 80);

  fill(100, 200, 200);
  ellipse(200, 200, 80, 80);

  fill(200, 200, 100);
  ellipse(175, 240, 80, 80);

  stroke(0);
  strokeWeight(5);
  
  point(300, 300);
  point(310, 300);
  point(320, 300);
  
  point(305, 310);
  point(315, 310);

  point(310, 320);

}

function draw() {
  
}
