function setup() {
  
  createCanvas(500, 500);
  
  strokeWeight(5); // 線の太さを5に設定
  
  stroke(0); // 黒色
  line(10, 0, 10, 200);
  
  stroke(125) // 灰色
  line(20, 0, 20, 210);
  
  stroke(255, 0, 0); // 赤色
  line(30, 0, 30, 220);
  
  stroke(0, 0, 255); // 青色
  line(40, 0, 40, 230); 
  
  stroke("green"); // 緑色
  line(50, 0, 50, 240);
  
  stroke("#00ffff"); // 水色 16進数で指定
  line(60, 0, 60, 250);
  
  stroke("rgb(255, 0, 255)"); // 紫色 CSSのrgb命令と同じ指定方法
  line(70, 0, 70, 260);
  
}

function draw() {
  
}
