
function Person(name, age) {
    this.name = name;
    this.age = age;
    this.sayHello = function() {
        println("NAME: "+ this.name + "\t AGE:" + this.age);
    };
}

function setup() {

    var bob = new Person("Bob", 43);

    // 存在するプロパティへのアクセス
    console.log(bob.name);
    console.log(bob["name"]);
    console.log(bob.age);

    // 存在しないプロパティへのアクセス
    console.log(bob.job);
    console.log(bob["job"]);


}

