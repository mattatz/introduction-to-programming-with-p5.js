
function Person() {
    console.log("Personのコンストラクタ");
}

Person.prototype.genus = "human";

Person.prototype.sayHello = function() {
    console.log("わたしはPersonです");
};

function Child() {
    console.log("Childのコンストラクタ");
}

Child.prototype.play = function() {
    console.log("Childは遊ぶ");
};

Object.setPrototypeOf(Child.prototype, Person.prototype); // ChildのプロトタイプにPersonのプロトタイプを設定する
// Child.prototype = Object.create(Person.prototype); // ChildのプロトタイプにPersonのプロトタイプを設定する

function Adult() {
}

Adult.prototype.work = function() {
    console.log("働くAdult");
};

Object.setPrototypeOf(Adult.prototype, Child.prototype);

function setup() {

    var otona = new Adult();
    otona.sayHello();
    otona.play();
    otona.work();

}

