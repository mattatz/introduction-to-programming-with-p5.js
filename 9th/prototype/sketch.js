
function Person() {
}

Person.prototype.genus = "human";

Person.prototype.sayHello = function() {
    console.log("わたしはPersonです");
};

function setup() {

    console.log(Person.genus); // undefinedと出力される

    var p = new Person(); // Person関数をコンストラクタとして呼び出す
    console.log(p.genus); // humanと出力される

    p.sayHello();

    var p2 = new Person();
    p2.sayHello();

}

