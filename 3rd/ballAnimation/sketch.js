
// ボールの位置を保存する変数
var positionX;
var positionY;

// ホールの進行方向を保存する変数
var vx;
var vy;

function setup() {
    createCanvas(500, 500);
  
    // (x, y)座標の初期化
    positionX = 50;
    positionY = 50;

    // 進行方向の初期化
    vx = 1;
    vy = 1;
}

function draw() {
    background(color(255, 255, 255)); // 白く画面全体を塗りつぶし、前の描画結果を消す

    noFill();
    stroke(color(0, 0, 0));
    strokeWeight(2);
    rect(0, 0, width, height); // キャンバスの黒い枠線を引く

    fill(color(0, 0, 255)); // 赤く塗りつぶす
    noStroke(); // 線を引かない
    ellipse(positionX, positionY, 20, 20); // (x, y)の位置に横幅80、縦幅80で円を描画する

    // 進行方向を変えるプログラムをここに書く！
    
    positionX += vx; // x座標をvx分移動させる
    positionY += vy; // y座標をvy分移動させる
}

