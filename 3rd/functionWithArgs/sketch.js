function setup() {
  
  createCanvas(500, 500);
  drawCrossMark(0, 0, 100, 100);
  
}

function draw() {
  
}

function drawCrossMark (x1, y1, x2, y2) {
  line(x1, y1, x2, y2);
  line(x2, y1, x1, y2);
}
