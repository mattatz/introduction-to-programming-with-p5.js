
// ボールの位置を保存する変数
var x;
var y;

function setup() {
  createCanvas(500, 500);
  
  // createCanvasを実行した後は、widthに横幅、heightに縦幅の値が格納される
  // ボールの位置をcanvasの真ん中に設定する
  x = width / 2; 
  y = height / 2;
}

function draw() {
  background(color(255, 255, 255)); // 白く画面全体を塗りつぶし、前の描画結果を消す
  fill(color(255, 0, 0)); // 赤く塗りつぶす
  ellipse(x, y, 80, 80); // (x, y)の位置に横幅80、縦幅80で円を描画する
  x++; // x座標を1ずつ増やしていく
}

