
function setup() {

    // sayHello関数の定義
    var sayHello = function() {
        // sayHello関数が呼び出された際に、
        // sayHello関数を格納していたオブジェクトのname変数をコンソールに出力する
        println("Hello, I'm " + this.name);
    };

    var alice = {
        name: 'Alice',
        sayHello: sayHello
    };

    var bob = {
        name: 'Bob',
        sayHello: sayHello
    };

    alice.sayHello();   // Hello, I'm Alice
    bob.sayHello();     // Hello, I'm Bob
  
}

