
/*
 * 球を表すオブジェクト
 * */
function Ball(x, y, size, col) {
    this.x = x;
    this.y = y;
    this.vx = random(-1, 1);
    this.vy = random(-1, 1);
    this.size = size;
    this.col = col;

    this.update = function() {
        this.x += this.vx;
        this.y += this.vy;
    };

    this.rebound = function() {
        if(this.x < 0 || this.x > width) {
            this.vx *= -1;
        }
        
        if(this.y < 0 || this.y > height) {
            this.vy *= -1;
        }
    };

    this.display = function() {
        fill(this.col);
        ellipse(this.x, this.y, this.size, this.size);
    };
}

var balls = [];

function setup() {
    createCanvas(500, 500);

    for(var i = 0; i < 50; i++) {
        var ball = new Ball(
            random(width), 
            random(height), 
            random(2, 10),
            color(random(255), random(255), random(255))
        );
        balls.push(ball);
    }
}

function draw() {
    background(255);

    noFill();
    stroke(color(0, 0, 0));
    rect(0, 0, width - 1, height - 1);
    
    for(var i = 0; i < balls.length; i++) {
        var ball = balls[i];
        ball.update();  // ボールを移動
        ball.rebound(); // ボールがCanvasの枠に跳ね返る
        ball.display(); // ボールを画面に表示する
    }
}

/*
 * new Ball()とコンストラクタを呼び出すだけで、
 * プログラム内のどこからでも同じBallオブジェクトを生成できる
 * */

function mousePressed() {
    var newBall = new Ball(mouseX, mouseY, random(3, 10), color(255, 0, 0));
    balls.push(newBall);
}

function mouseDragged() {
    var newBall = new Ball(mouseX, mouseY, random(3, 10), color(255, 0, 0));
    balls.push(newBall);
}

