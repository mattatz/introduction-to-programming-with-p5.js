
function Person(name, age) {
    this.name = name;
    this.age = age;
    this.sayHello = function() {
        println("NAME: "+ this.name + "\t AGE:" + this.age);
    };
}

function setup() {

    var alice = new Person("Alice", 7);
    alice.sayHello();

    var bob = new Person("Bob", 42);
    bob.sayHello();

}

