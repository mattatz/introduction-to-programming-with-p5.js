
var img;

function setup() {
    createCanvas(500, 500);

    // wikipediaのサーバに置いてある画像を読み込んでくる
    var url = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Processing_Logo_Clipped.svg/256px-Processing_Logo_Clipped.svg.png";
    img = loadImage(url);
}

function draw() {
    image(img, 0, 0, 128, 128);
    image(img, 128, 0, 256, 128);
}


