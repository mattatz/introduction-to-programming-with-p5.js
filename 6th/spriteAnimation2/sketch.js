
var sprites = [];

function preload() {
    sprites[0] = loadImage("fly_01.png");
    sprites[1] = loadImage("fly_02.png");
    sprites[2] = loadImage("fly_03.png");
}

function setup() {
    createCanvas(500, 500);
    frameRate(10);
}

function draw() {
    background(255);
    image(sprites[frameCount % sprites.length], 0, 0, 128, 128);
}


