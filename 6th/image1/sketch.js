
var img;

function setup() {
    createCanvas(500, 500);

    img = loadImage("p5.png");

    /*
    image(img, 0, 0, 128, 128);
    image(img, 128, 0, 256, 128);
    */
}

function draw() {
    image(img, 0, 0, 128, 128);
    image(img, 128, 0, 256, 128);
}


