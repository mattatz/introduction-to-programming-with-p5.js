
var img;

/*
 * setupの前に実行される関数
 * プログラムを止めてしまうような通信処理などはこの関数の中で行う。
 * preload関数の中で書かれた処理が終わるまでsetup関数は実行されない。
 * */
function preload() {
    img = loadImage("p5.png");
}

function setup() {
    createCanvas(500, 500);

    image(img, 0, 0, 128, 128);
    image(img, 128, 0, 256, 128);
}

function draw() {
}


