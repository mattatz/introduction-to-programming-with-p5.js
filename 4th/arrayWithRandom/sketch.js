
var xs = [];
var ys = [];
var rs = [];

function setup() {
  
  createCanvas(500, 500);
  
  for(var i = 0; i < 50; i++) {
    var x = random(width); // 0からwidthまでのランダムな値
    var y = random(height); // 0からheightまでのランダムな値
    var r = random(5, 20); // 5から20までのランダムな値
    xs.push(x);
    ys.push(y);
    rs.push(r);
  }
  
}

function draw() {
  background(255);

  for(var i = 0; i < xs.length; i++) {
    var x = xs[i];
    var y = ys[i];
    var r = rs[i];
    ellipse(x, y, r, r);
  }
}

