/*
 * drawWithLoop
 * 繰り返しを用いた描画のサンプル
 * */

function setup() {

    createCanvas(500, 500);

    background(0);

    /*
     * 繰り返しを用いてグリッド線を描画する
     * */

    stroke(255, 10);
    strokeWeight(2);

    // width, heightはcreateCanvasを実行したときにp5.jsが
    // 自動的に生成してくれるcanvasの横幅と縦幅の値が入った変数
    var dx = width / 40;
    var dy = height / 40;

    // 横線
    for(var y = 0; y < height; y += dy) {
        line(0, y, width, y);
    }

    // 縦線
    for(var x = 0; x < width; x += dx) {
        line(x, 0, x, height);
    }

    /*
     * 円周に沿って円を配置する
     * */

    // Math.PIは円周率πの値
    var pi2 = Math.PI * 2;
    var step = Math.PI * 2 / 50;

    noStroke();

    var cx = width / 2; // canvasの真ん中のx座標
    var cy = height / 2; // canvasの真ん中のy座標
    var w = width / 4; // 円の横幅
    var h = height / 4; // 円の縦幅

    var from = color(255, 0, 0, 100);
    var to = color(0, 0, 255, 100);

    // 変数rはpi2に達するまでstepずつ増えていく
    for(var r = 0; r < pi2; r += step) {

        var x = Math.cos(r) * w;
        var y = Math.sin(r) * h;

        // 色変数fromとtoとの間を補間した色をlerpColorで計算する
        var c = lerpColor(from, to, r / pi2);
        fill(c);

        ellipse(cx + x, cy + y, 10, 10);
    }

}

function draw() {
  
}
