/*
 * sketch.js
 * */

// 一番初めに実行されるブロック
function setup() {

    createCanvas(500, 500); // canvasのサイズを(width, height) = (500, 500)に設定する

    // 点の位置をずらす
    var offset = 5;

    // 点同士の幅の長さ
    var len = 30;

    // 点の太さ
    strokeWeight(10);

    for(var y = 0; y < 9; y++) {
        for(var x = 0; x < 9; x++) {

            // ここでxとyの値に応じた条件分岐を書き、
            // 点の色を変えましょう！
            stroke(color(0, 0, 0));

            // 点を打つ
            point(x * len + offset, y * len + offset);
        }
    }

}

// ループ実行されるブロック
function draw() {
  
}
