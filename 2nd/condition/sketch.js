function setup() {

    if(true) {
        println("最初の括弧の中身");
    } else {
        println("後の括弧の中身");
    }

    if(3 > 1) {
        println("3は1よりも大きい");
    } else {
        println("3は1よりも小さい");
    }

    if(10 == 100) {
        println("10 == 100");
    } else if(50 > 500) {
        println("50 > 500");
    } else if(90 == (45 * 2)) {
        println("90 == (45 * 2)");
    } else {
        println("else");
    }

}

function draw() {
  
}
