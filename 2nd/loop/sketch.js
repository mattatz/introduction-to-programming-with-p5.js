function setup() {

    // iが2ずつ増えるfor文での繰り返し
    for(var i = 0; i < 20; i += 2) {
        println(i);
    }

    var i = 0;
    while(i < 10) {
        if(i >= 5) {
            println("iが5よりも大きい");
        } else {
            println("iが5よりも少ない");
        }
        i++; // インクリメント : i+=1と同じ処理
    }
  
}

function draw() {
  
}
