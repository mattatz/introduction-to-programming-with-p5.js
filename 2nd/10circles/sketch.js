/*
 * sketch.js
 * */

// 一番初めに実行されるブロック
function setup() {

    createCanvas(500, 500); // canvasのサイズを(width, height) = (500, 500)に設定する

    var x = 200; // x軸上の位置
    var y = 200; // y軸上の位置

    // i++; // 1ずつ足す // i += 1と同じ
    // i--; // 1ずつ引く // i -= 1と同じ

    noFill(); // 線だけ描く！

    var r = 10; // 半径

    for(var i = 0; i < 10; i += 1) {

        if(i < 5) {
            stroke(color(255, 0, 0));
        } else {
            stroke(color(0, 0, 255));
        }

        ellipse(200, 200, r, r); // 円を描く命令 ellipse
        r += 10;
    }

}

// ループ実行されるブロック
function draw() {
  
}
