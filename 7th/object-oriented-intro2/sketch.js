
function Ball(x, y, r, vx, vy) {

    this.x = x;
    this.y = y;
    this.r = r;
    this.vx = vx;
    this.vy = vy;

    this.update = function() {
        this.x = this.x + this.vx;
        this.y = this.y + this.vy;
    };

    this.rebound = function(w, h) {
        if(this.x < 0 || this.x > w) {
            this.vx = - this.vx;
        }

        if(this.y < 0 || this.y > h) {
            this.vy = - this.vy;
        }
    };

}

var balls = [];

function setup() {
    createCanvas(500, 500);

    for(var i = 0; i < 50; i++) {
        var x = random(0, width);
        var y = random(0, height);
        var r = random(2, 5);
        var vx = random(-1, 1);
        var vy = random(-1, 1);

        var ball = new Ball(x, y, r, vx, vy);
        balls.push(ball);
    }

}

function draw() {
    background(255);

    // 枠線
    rect(0, 0, width - 1, height - 1);

    for(var i = 0; i < balls.length; i++) {
        var ball = balls[i];

        ellipse(ball.x, ball.y, ball.r, ball.r);
        ball.update();
        ball.rebound(width, height);
    }
}


