
function setup() {

    createCanvas(500, 500);

    // 連想配列のサンプル
    // メニュー名をキー、メニューの値段を値で設定する
    
    var prices = {
        "コーヒー"  : 300,
        "ビール"    : 550,
        "EDAMAME"   : 300,
        "SASHIMI"   : 700,
        sake        : 800 // アルファベットであればキーを""で囲む必要はない
    };

    println(prices["コーヒー"]);    // 300
    println(prices.SASHIMI);        // 700
    println(prices.sake);           // 800

    println(prices);

    prices["EDAMAME"] = 500;// 値の上書き
    prices.sake = 1200;     // 値の上書き
    prices.tea = 450;       // 新しいキーと値の追加

    println(prices);        

}

