
var ball = {};

function setup() {

    createCanvas(500, 500);

    ball.x = 100;
    ball.y = 100;
    ball.radius = 50; 
    ball.color = color(255, 0, 0);

}

function draw() {
    background(255);

    fill(ball.color);
    ellipse(ball.x, ball.y, ball.radius, ball.radius);
}

