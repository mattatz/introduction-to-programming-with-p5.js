
var xs = []; // x座標
var ys = []; // y座標
var rs = []; // ボールの直径
var vxs = []; // x方向の進み具合
var vys = []; // y方向の進み具合

function setup() {
    createCanvas(500, 500);

    for(var i = 0; i < 50; i++) {
        xs.push(random(0, width));
        ys.push(random(0, height));
        rs.push(random(2, 5));
        vxs.push(random(-1, 1));
        vys.push(random(-1, 1));
    }

}

function draw() {
    background(255);

    // 枠線
    rect(0, 0, width - 1, height - 1);

    for(var i = 0; i < xs.length; i++) {
        var x = xs[i];
        var y = ys[i];
        var r = rs[i];
        var vx = vxs[i];
        var vy = vys[i];
        ellipse(x, y, r, r);

        xs[i] = x + vx;
        ys[i] = y + vy;
        
        if(xs[i] < 0 || xs[i] > width) {
            vxs[i] = - vxs[i];
        }

        if(ys[i] < 0 || ys[i] > height) {
            vys[i] = - vys[i];
        }
    }
}


