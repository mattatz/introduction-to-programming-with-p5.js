
var balls = [];

function setup() {
    createCanvas(500, 500);

    for(var i = 0; i < 50; i++) {
        var ball = {};
        ball.x = random(0, width);
        ball.y = random(0, height);
        ball.radius = random(2, 5); 
        ball.color = color(random(255), random(255), random(255));
        balls.push(ball);
    }
}

function draw() {
    background(255);
    noStroke();
    for(var i = 0; i < balls.length; i++) {
        var ball =  balls[i];
        fill(ball.color);
        ellipse(ball.x, ball.y, ball.radius, ball.radius);
    }
}

