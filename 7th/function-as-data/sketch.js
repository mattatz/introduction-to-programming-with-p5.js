
function setup() {

    createCanvas(500, 500);

    // 数値型のデータをnumber変数に代入
    var number = 1234;

    // 関数型のデータをplus変数に代入
    var plus = function(a, b) {
        return a + b;
    };

    // 関数型のデータをminus変数に代入
    function minus(a, b) {
        return a - b;
    };

    println(number);
    println(plus);
    println(plus(1, 1));
    println(minus);
    println(minus(5, 2));

}

