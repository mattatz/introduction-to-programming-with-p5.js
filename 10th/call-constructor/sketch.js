
function Ball(x, y, size, col) {
    this.x = x;
    this.y = y;
    this.vx = random(-1, 1);
    this.vy = random(-1, 1);
    this.size = size;
    this.col = col;
}

Ball.prototype.update = function() {
    this.x += this.vx;
    this.y += this.vy;
};

Ball.prototype.rebound = function() {
    if(this.x < 0 || this.x > width) {
        this.vx *= -1;
    }
    
    if(this.y < 0 || this.y > height) {
        this.vy *= -1;
    }
};

Ball.prototype.display = function() {
    push();
    fill(this.col);
    ellipseMode(CENTER);
    ellipse(this.x, this.y, this.size, this.size);
    pop();
};

function SquareBall(x, y, size, col) {
    // function Ball(x, y, size, col) { ... } の中に記述されている処理を子クラスの
    // SquareBallのコンストラクタ内でも実行する
    Ball.call(this, x, y, size, col); 
}

Object.setPrototypeOf(SquareBall.prototype, Ball.prototype);

SquareBall.prototype.display = function() {
    push();
    fill(this.col);
    rectMode(CENTER);
    rect(this.x, this.y, this.size, this.size);
    pop();
};

var balls = [];

function setup() {

    createCanvas(500, 500);

    for(var i = 0; i < 50; i++) {
        var ball = new SquareBall(
            random(width), 
            random(height), 
            random(4, 10),
            color(random(255), random(255), random(255))
        );
        balls.push(ball);
    }

}

function draw() {

    background(255);

    noFill();
    stroke(color(0, 0, 0));
    rect(0, 0, width - 1, height - 1);
    
    for(var i = 0; i < balls.length; i++) {
        var ball = balls[i];
        ball.update();  // ボールを移動
        ball.rebound(); // ボールがCanvasの枠に跳ね返る
        ball.display(); // ボールを画面に表示する
    }

}

