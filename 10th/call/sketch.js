
function test1() {
    this.label = "test1";
}

function test2() {
    this.label = "test2";
}

function Test() {
}

function setup() {

    var t = new Test();
    console.log(t);

    test1.call(t); // call関数をつけてtest1関数を呼ぶことで、test1関数内のオブジェクトをtに設定する
    console.log(t);

    test2.call(t);
    console.log(t);

}

